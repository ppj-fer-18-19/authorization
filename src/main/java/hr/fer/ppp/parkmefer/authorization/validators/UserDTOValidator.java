package hr.fer.ppp.parkmefer.authorization.validators;

import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.function.IntPredicate;

@Component
public class UserDTOValidator implements Validator {

    private static final int MAX_FIELD_LENGTH = 128;
    private static final int MIN_PASSWORD_SIZE = 8;
    private static final int MAX_PASSWORD_SIZE = 50; //Bycript encoder max length

    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String PASSWORD = "password";
    private static final String PHONE = "phone";
    private static final String ROLE = "role";

    private static final IntPredicate IS_NAME_PREDICATE = c -> Character.isAlphabetic(c) || c == ' ' || c == '-';
    private static final IntPredicate IS_DIGIT_PREDICATE = Character::isDigit;
    private static final String APP_USER_FORM_NAME_EMPTY = "appUserForm.name.empty";
    private static final String APP_USER_FORM_NAME_TOO_LONG = "appUserForm.name.tooLong";
    private static final String APP_USER_FORM_NAME_BAD_FORMAT = "appUserForm.name.badFormat";
    private static final String APP_USER_FORM_SURNAME_EMPTY = "appUserForm.surname.empty";
    private static final String APP_USER_FORM_SURNAME_TOO_LONG = "appUserForm.surname.tooLong";
    private static final String APP_USER_FORM_SURNAME_BAD_FORMAT = "appUserForm.surname.badFormat";
    private static final String APP_USER_FORM_PASSWORD_EMPTY = "appUserForm.password.empty";
    private static final String APP_USER_FORM_PASSWORD_TOO_SHORT = "appUserForm.password.tooShort";
    private static final String APP_USER_FORM_PASSWORD_TOO_LONG = "appUserForm.password.tooLong";
    private static final String APP_USER_FORM_PHONE_WRONG_FORMAT = "appUserForm.phone.wrongFormat";
    private static final String APP_USER_FORM_PHONE_WRONG_FORMAT1 = "appUserForm.phone.wrongFormat";


    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;

        prepareFields(user);

        validateName(user, errors);
        validateSurname(user, errors);
        validatePassword(user, errors);
        validateTelNumber(user, errors);
    }

    /**
     * Method checks the validity of the name field.
     */
    private void validateName(UserDTO user, Errors errors) {
        String name = user.getName();

        if (name == null || name.isEmpty()) {
            errors.rejectValue(NAME, APP_USER_FORM_NAME_EMPTY);
        } else if (name.length() > MAX_FIELD_LENGTH) {
            errors.rejectValue(NAME, APP_USER_FORM_NAME_TOO_LONG);
        } else if (!name.chars().allMatch(IS_NAME_PREDICATE)) {
            errors.rejectValue(NAME, APP_USER_FORM_NAME_BAD_FORMAT);
        }
    }

    /**
     * Method checks the validity of the surname field.
     */
    private void validateSurname(UserDTO user, Errors errors) {
        String surname = user.getSurname();

        if (surname == null || surname.isEmpty()) {
            errors.rejectValue(SURNAME, APP_USER_FORM_SURNAME_EMPTY);
        } else if (surname.length() > MAX_FIELD_LENGTH) {
            errors.rejectValue(SURNAME, APP_USER_FORM_SURNAME_TOO_LONG);
        } else if (!surname.chars().allMatch(IS_NAME_PREDICATE)) {
            errors.rejectValue(SURNAME, APP_USER_FORM_SURNAME_BAD_FORMAT);
        }
    }

    /**
     * Method checks the validity of the password field.
     */
    private void validatePassword(UserDTO user, Errors errors) {
        String password = user.getPassword();

        if (password == null || password.length() == 0) {
            errors.rejectValue(PASSWORD, APP_USER_FORM_PASSWORD_EMPTY);
        } else if (password.length() < MIN_PASSWORD_SIZE) {
            errors.rejectValue(PASSWORD, APP_USER_FORM_PASSWORD_TOO_SHORT);
        } else if (password.length() > MAX_PASSWORD_SIZE) {
            errors.rejectValue(PASSWORD, APP_USER_FORM_PASSWORD_TOO_LONG);
        }
    }

    /**
     * Method checks the validity of the phone field.
     */
    private void validateTelNumber(UserDTO user, Errors errors) {
        String phone = user.getPhone();

        if (phone != null && !phone.isEmpty()) {
            if (!phone.startsWith("+")) {
                errors.rejectValue(PHONE, APP_USER_FORM_PHONE_WRONG_FORMAT);
            }

            if (!phone.chars().skip(1L).allMatch(IS_DIGIT_PREDICATE)) {
                errors.rejectValue(PHONE, APP_USER_FORM_PHONE_WRONG_FORMAT1);
            }
        }
    }

    /**
     * Changes fields that are null to empty strings and trims the fields. Fields that are checked
     * are name, surname, email and telNumber.
     */
    private static void prepareFields(UserDTO user) {
        user.setName(user.getName() == null ? "" : user.getName().trim());
        user.setSurname(user.getSurname() == null ? "" : user.getSurname().trim());
        user.setEmail(user.getEmail() == null ? "" : user.getEmail().trim());

        String phone = user.getPhone();
        if (phone != null) {
            if (phone.startsWith("00")) phone.replaceFirst("00", "+");
            user.setPhone(phone
                    .replaceAll("\\s+", "")
                    .replaceAll("-", "")
                    .replaceAll("/", ""));

        }
    }
}
