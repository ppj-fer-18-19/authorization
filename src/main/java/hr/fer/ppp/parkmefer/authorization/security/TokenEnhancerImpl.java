package hr.fer.ppp.parkmefer.authorization.security;

import hr.fer.ppp.parkmefer.authorization.dto.jwt.UserTokenData;
import hr.fer.ppp.parkmefer.authorization.entities.User;
import hr.fer.ppp.parkmefer.authorization.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

@Component
@Primary
public class TokenEnhancerImpl implements TokenEnhancer {

    @Value("${app.security.jwt-data-key}")
    private String USER_DATA_KEY;

    private final UserRepository userRepository;

    @Autowired
    public TokenEnhancerImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        UserTokenData userTokenData = getUserData(authentication);

        additionalInfo.put(USER_DATA_KEY, userTokenData);

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

    /**
     * @param authentication OAuth2Authentication object
     * @return enhanced token data object
     */
    private UserTokenData getUserData(OAuth2Authentication authentication) {
        UserTokenData userTokenData = new UserTokenData();
        User user = userRepository.findByEmail(authentication.getName()).orElseThrow(EntityNotFoundException::new);

        userTokenData.setEmail(authentication.getName());
        userTokenData.setId(user.getID());
        userTokenData.setRoleId(user.getRoleId());

        return userTokenData;
    }
}