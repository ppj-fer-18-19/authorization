package hr.fer.ppp.parkmefer.authorization.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class InviteDTO {

    @NotEmpty(message = "appUserForm.email.empty")
    @Email(message = "appUserForm.email.wrongFormat")
    private String email;

    @NotNull(message = "common.form.empty")
    @Min(value = 1, message = "common.form.entityNotFound")
    @Max(value = 2, message = "common.form.entityNotFound")
    private Integer roleId;
}
