package hr.fer.ppp.parkmefer.authorization.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
@Data
public class UserDTO {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID ID;

    private String name;

    private String surname;

    private String email;

    private String phone;

    private String password;

    private Integer roleId;
}
