package hr.fer.ppp.parkmefer.authorization.repositories;

import hr.fer.ppp.parkmefer.authorization.entities.Invite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface InvitationRepository extends JpaRepository<Invite, UUID> {
    Optional<Invite> findByEmail(String email);
}
