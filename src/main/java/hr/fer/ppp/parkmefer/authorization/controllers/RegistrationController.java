package hr.fer.ppp.parkmefer.authorization.controllers;

import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import hr.fer.ppp.parkmefer.authorization.entities.Invite;
import hr.fer.ppp.parkmefer.authorization.services.UserService;
import hr.fer.ppp.parkmefer.authorization.validators.UserDTOValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class RegistrationController implements ErrorController {

    private static final String REGISTRATION_VIEW = "registration";
    private static final String WELLCOME_VIEW = "wellcome";
    private static final String ERROR_VIEW = "error";
    private static final String ERROR_PATH = "/error";

    private static final String USER_KEY = "user";
    private static final String NAME_KEY = "name";
    private static final String SURNAME_KEY = "surname";

    private final UserService userService;
    private final UserDTOValidator userDTOValidator;

    @Value("${app.services.frontend}")
    private String FRONTEND_LINK;

    @Autowired
    public RegistrationController(UserService userService, UserDTOValidator userDTOValidator) {
        this.userService = userService;
        this.userDTOValidator = userDTOValidator;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister(Model model, @RequestParam(value = "key") UUID key) {
        Invite invite = userService.getInviteById(key);

        UserDTO dto = new UserDTO();
        dto.setEmail(invite.getEmail());
        dto.setRoleId(invite.getRoleId());

        model.addAttribute(USER_KEY, dto);
        return REGISTRATION_VIEW;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String addUser(Model model, @ModelAttribute("user") @Valid UserDTO user, BindingResult result) {
        if (result.hasErrors()) {
            return REGISTRATION_VIEW;
        } else {
            userService.registerUser(user);

            model.addAttribute(NAME_KEY, user.getName());
            model.addAttribute(SURNAME_KEY, user.getSurname());

            return WELLCOME_VIEW;
        }
    }

    @RequestMapping(ERROR_PATH)
    public String error() {
        return ERROR_VIEW;
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    public String getFrontendLink() {
        return FRONTEND_LINK;
    }

    @InitBinder("user")
    private void initUserDTOBinder(WebDataBinder binder) {
        binder.addValidators(userDTOValidator);
    }
}
