package hr.fer.ppp.parkmefer.authorization.services.impl;

import hr.fer.ppp.parkmefer.authorization.dto.InviteDTO;
import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import hr.fer.ppp.parkmefer.authorization.dto.mappers.impl.UserMapper;
import hr.fer.ppp.parkmefer.authorization.entities.Invite;
import hr.fer.ppp.parkmefer.authorization.entities.User;
import hr.fer.ppp.parkmefer.authorization.repositories.InvitationRepository;
import hr.fer.ppp.parkmefer.authorization.repositories.UserRepository;
import hr.fer.ppp.parkmefer.authorization.services.UserService;
import hr.fer.ppp.parkmefer.authorization.services.util.mail.CustomMailSender;
import hr.fer.ppp.parkmefer.authorization.services.util.mail.model.InvitationEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Supplier;

@Service
@Primary
public class UserServiceImpl implements UserService {

    private static final Supplier<RuntimeException> ENTITY_NOT_FOUND = EntityNotFoundException::new;
    private static final Supplier<RuntimeException> UNAUTHORIZED_USER = () -> new UnauthorizedUserException("User is unauthorized.");

    private static final String INVITATION_LINK_KEY = "#INVITATION_LINK#";
    private static final String ADMIN_EMAIL_KEY = "#ADMIN_EMAIL#";
    private static final String FRONTEND_LINK_KEY = "#FRONTEND_LINK#";
    private static final String INVITATION_LINK_PATTERN = "%sregister?key=%s";
    private static final String SUBJECT = "ParkirajMeFER - Pozivnica";
    private static final String USERNAME_NOT_FOUND = "User with email %s has not been registered.";

    @Value("${spring.mail.username}")
    private String APPLICATION_EMAIL;
    @Value("${app.services.frontend}")
    private String FRONTEND_LINK;
    @Value("${app.services.authorization}")
    private String AUTHORIZATION_LINK;

    private final UserRepository userRepository;
    private final InvitationRepository invitationRepository;
    private final CustomMailSender customMailSender;
    private final InvitationEmail invitationEmail;
    private final UserMapper userMapper;
    private final Function<? super User, ? extends UserDTO> USER_MAPPING_FUNCTION;

    @Autowired
    public UserServiceImpl(UserMapper userMapper, UserRepository userRepository, InvitationRepository invitationRepository, CustomMailSender customMailSender, InvitationEmail invitationEmail) {
        this.userRepository = userRepository;
        this.invitationRepository = invitationRepository;
        this.customMailSender = customMailSender;
        this.invitationEmail = invitationEmail;
        this.userMapper = userMapper;

        this.USER_MAPPING_FUNCTION = userMapper::entityToDto;
    }

    @Override
    public UserDTO getUserByEmail(@NotNull String email) {
        return userMapper.entityToDto(userRepository.findByEmail(email).orElseThrow(ENTITY_NOT_FOUND));
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElse(null);

        if (user == null) {
            throw new UsernameNotFoundException(String.format(USERNAME_NOT_FOUND, email));
        }

        Set<GrantedAuthority> auth = new HashSet<>();
        auth.add(new SimpleGrantedAuthority(user.getRole().getName()));

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPasswordHash(), auth);
    }

    @Override
    public Invite getInviteById(UUID id) {
        return invitationRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND);
    }

    @Override
    public boolean inviteNewUser(@NotNull InviteDTO inviteDTO) {
        User activeUser = getActiveUserInternal();

        Invite invite = new Invite();
        invite.setEmail(inviteDTO.getEmail());
        invite.setRoleId(inviteDTO.getRoleId());

        if (!invitationRepository.findByEmail(inviteDTO.getEmail()).isPresent()) {
            invitationRepository.save(invite);
        } else {
            return false;
        }

        invitationEmail.setArgument(INVITATION_LINK_KEY, String.format(INVITATION_LINK_PATTERN, AUTHORIZATION_LINK, invite.getId()));
        invitationEmail.setArgument(FRONTEND_LINK_KEY, FRONTEND_LINK);
        invitationEmail.setArgument(ADMIN_EMAIL_KEY, activeUser.getEmail());

        try {
            customMailSender
                    .create()
                    .setFrom(APPLICATION_EMAIL)
                    .setTo(invite.getEmail())
                    .setSubject(SUBJECT)
                    .send(invitationEmail, true);
        } catch (Exception e) {
            invitationRepository.delete(invite);
            return false;
        }

        return true;
    }

    @Override
    public UserDTO getActiveUser() {
        return userMapper.entityToDto(getActiveUserInternal());
    }

    @Override
    public Page<UserDTO> getUsersPageable(Pageable pageable) {
        return userRepository.findAll(pageable).map(USER_MAPPING_FUNCTION);
    }

    @Override
    public void deleteUserById(@NotNull UUID id) {
        userRepository.deleteById(id);
    }

    @Override
    public void registerUser(UserDTO userDTO) {
        User user = userMapper.dtoToEntity(userDTO);

        userRepository.save(user);
    }

    private User getActiveUserInternal() {
        return userRepository
                .findByEmail(SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).orElseThrow(UNAUTHORIZED_USER);
    }
}
