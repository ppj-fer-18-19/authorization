package hr.fer.ppp.parkmefer.authorization.services;

import hr.fer.ppp.parkmefer.authorization.dto.InviteDTO;
import hr.fer.ppp.parkmefer.authorization.dto.UserDTO;
import hr.fer.ppp.parkmefer.authorization.entities.Invite;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    /**
     * @param email users email
     * @return user with provided email
     */
    UserDTO getUserByEmail(@NotNull String email);

    /**
     * @param id invitation id
     * @return invite with provided id
     */
    Invite getInviteById(@NotNull UUID id);

    /**
     * @param invite invite
     */
    boolean inviteNewUser(@NotNull InviteDTO invite);

    /**
     * @return user who sent request
     */
    UserDTO getActiveUser();

    /**
     * @param userDTO dto containing information about user to be registered
     */
    void registerUser(UserDTO userDTO);

    /**
     * @param id id of user to be deleted
     */
    void deleteUserById(@NotNull UUID id);

    /**
     * @param pageable page request
     * @return list of users
     */
    Page<UserDTO> getUsersPageable(Pageable pageable);
}
